# traffic-control
This repository contains variations (implementation stages) of a basic traffic control system

* Variation 1
  * Simple Traffic Control System for a 4-way intersection of 2-lane roads
  
* Variation 2
  * Extension for manual traffic control and emergency-response mode
  * Application of State pattern

* Variation 3
  * Extension to support easy configuration of a traffic control system for any intersection.
  * Application of State-Transition pattern

* Variation 4
  * Extension for coordination with neighboring traffic control systems
