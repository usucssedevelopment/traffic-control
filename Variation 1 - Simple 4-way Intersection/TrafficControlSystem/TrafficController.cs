﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace TrafficControlSystem
{
    public class TrafficController
    {
        public enum PossibleStates { AllStopped1, AllowABCD, SlowABCD, AllStopped2, AllowEFGH, SlowEFGH }

        private readonly Dictionary<PossibleStates, string> _stateNames = new Dictionary<PossibleStates, string>()
        {
            {PossibleStates.AllStopped1, "All Traffic Stopped"},
            {PossibleStates.AllowABCD, "Allow Traffic for A, B, C, and D"},
            {PossibleStates.SlowABCD, "Slow Traffic for A, B, C, and D"},
            {PossibleStates.AllStopped2, "All Traffic Stopped"},
            {PossibleStates.AllowEFGH, "Allow Traffic for E, F, G, and H"},
            {PossibleStates.SlowEFGH, "Slow Traffic for E, F, G, and H"}
        };

        private Thread _myThread;

        public void Start()
        {
            if (IsRunning)
                Stop();

            IsRunning = true;
            _myThread = new Thread(Run);
            _myThread.Start();

        }

        public void Stop()
        {
            IsRunning = false;
        }

        public PossibleStates CurrentState { get; private set; } = PossibleStates.AllStopped1;
        public string CurrentStateName => _stateNames[CurrentState];
        public bool IsRunning { get; private set; }

        private void Run()
        {
            while (IsRunning)
            {
                if (!Sleep(3)) break;
                CurrentState = PossibleStates.AllowABCD;

                if (!Sleep(30)) break;
                CurrentState = PossibleStates.SlowABCD;

                if (!Sleep(10)) break;
                CurrentState = PossibleStates.AllStopped2;

                if (!Sleep(3)) break;
                CurrentState = PossibleStates.AllowEFGH;

                if (!Sleep(30)) break;
                CurrentState = PossibleStates.SlowEFGH;

                if (!Sleep(10)) break;
                CurrentState = PossibleStates.AllStopped1;
            }
        }

        private bool Sleep(int seconds)
        {
            var startingTime = DateTime.Now;
            while (IsRunning && DateTime.Now.Subtract(startingTime).TotalSeconds < seconds)
                Thread.Sleep(10);

            return IsRunning;
        }


    }
}
