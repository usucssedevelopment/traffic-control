﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrafficControlSystem
{
    public partial class DisplayForm : Form
    {
        private readonly TrafficController _trafficController = new TrafficController();

        public DisplayForm()
        {
            InitializeComponent();
            DisplayStartStopButton();
        }

        private void startStopButton_Click(object sender, EventArgs e)
        {
            if (_trafficController.IsRunning)
            {
                _trafficController.Stop();
            }
            else
            {
                _trafficController.Start();
            }
            DisplayStartStopButton();
        }

        private void DisplayStartStopButton()
        {
            _startStopButton.Text = _trafficController.IsRunning ? "Stop Traffic Controller" : "Start Traffic Controller";
            _stateDisplay.Visible = _trafficController.IsRunning;
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            if (_trafficController.IsRunning)
                _stateDisplay.Text = _trafficController.CurrentStateName;
        }

        private void DisplayForm_Load(object sender, EventArgs e)
        {
            _refreshTimer.Start();
        }

        private void DisplayForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _refreshTimer.Stop();
        }
    }
}
