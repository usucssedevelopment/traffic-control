﻿namespace TrafficControlSystem
{
    partial class DisplayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startStopButton = new System.Windows.Forms.Button();
            this._stateDisplay = new System.Windows.Forms.Label();
            this._refreshTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // _startStopButton
            // 
            this._startStopButton.Location = new System.Drawing.Point(107, 12);
            this._startStopButton.Name = "_startStopButton";
            this._startStopButton.Size = new System.Drawing.Size(131, 23);
            this._startStopButton.TabIndex = 0;
            this._startStopButton.Text = "Start Traffic Controller";
            this._startStopButton.UseVisualStyleBackColor = true;
            this._startStopButton.Click += new System.EventHandler(this.startStopButton_Click);
            // 
            // _stateDisplay
            // 
            this._stateDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._stateDisplay.Location = new System.Drawing.Point(1, 61);
            this._stateDisplay.Name = "_stateDisplay";
            this._stateDisplay.Size = new System.Drawing.Size(336, 23);
            this._stateDisplay.TabIndex = 2;
            this._stateDisplay.Text = "(state)";
            this._stateDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _refreshTimer
            // 
            this._refreshTimer.Tick += new System.EventHandler(this.RefreshTimer_Tick);
            // 
            // DisplayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 114);
            this.Controls.Add(this._stateDisplay);
            this.Controls.Add(this._startStopButton);
            this.Name = "DisplayForm";
            this.Text = "Traffic Control System";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DisplayForm_FormClosing);
            this.Load += new System.EventHandler(this.DisplayForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _startStopButton;
        private System.Windows.Forms.Label _stateDisplay;
        private System.Windows.Forms.Timer _refreshTimer;
    }
}

