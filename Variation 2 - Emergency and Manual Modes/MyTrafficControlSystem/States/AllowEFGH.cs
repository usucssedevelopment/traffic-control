﻿using TrafficControl;

namespace MyTrafficControlSystem.States
{
    public class AllowEFGH : TrafficState
    {
        public AllowEFGH() : base(30) { }

        protected override TrafficState GetNextState()
        {
            return new SlowEFGH();
        }
    }
}
