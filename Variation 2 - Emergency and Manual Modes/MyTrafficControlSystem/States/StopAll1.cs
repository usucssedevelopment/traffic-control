﻿using TrafficControl;

namespace MyTrafficControlSystem.States
{
    public class StopAll1 : TrafficState
    {
        public StopAll1() : base(3) { }

        protected override TrafficState GetNextState()
        {
            return new AllowABCD();
        }
    }
}
