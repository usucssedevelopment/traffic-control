﻿using TrafficControl;

namespace MyTrafficControlSystem.States
{
    public class StopAll2 : TrafficState
    {
        public StopAll2() : base(3) { }

        protected override TrafficState GetNextState()
        {
            return new AllowEFGH();
        }
    }
}
