﻿using TrafficControl;

namespace MyTrafficControlSystem.States
{
    public class SlowABCD : TrafficState
    {
        public SlowABCD() : base(10) { }

        protected override TrafficState GetNextState()
        {
           return new StopAll2();
        }
    }
}
