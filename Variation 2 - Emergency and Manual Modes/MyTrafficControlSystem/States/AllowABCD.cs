﻿using TrafficControl;

namespace MyTrafficControlSystem.States
{
    public class AllowABCD : TrafficState
    {
        public AllowABCD() : base(30) {}

        protected override TrafficState GetNextState()
        {
            return new SlowABCD();
        }
    }
}
