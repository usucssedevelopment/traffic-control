﻿using TrafficControl;

namespace MyTrafficControlSystem.States
{
    public class SlowEFGH : TrafficState
    {
        public SlowEFGH() : base(10) { }

        protected override TrafficState GetNextState()
        {
            return new StopAll2();
        }
    }
}
