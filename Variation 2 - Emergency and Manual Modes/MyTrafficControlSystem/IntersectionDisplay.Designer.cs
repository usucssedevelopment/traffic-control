﻿namespace MyTrafficControlSystem
{
    partial class IntersectionDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._startStopButton = new System.Windows.Forms.Button();
            this._manualModeButton = new System.Windows.Forms.Button();
            this._emergencyButton = new System.Windows.Forms.Button();
            this.intersectionImage = new System.Windows.Forms.Panel();
            this._refreshTimer = new System.Windows.Forms.Timer(this.components);
            this._emergencySelection = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // _startStopButton
            // 
            this._startStopButton.Location = new System.Drawing.Point(21, 12);
            this._startStopButton.Name = "_startStopButton";
            this._startStopButton.Size = new System.Drawing.Size(75, 23);
            this._startStopButton.TabIndex = 0;
            this._startStopButton.Text = "Start";
            this._startStopButton.UseVisualStyleBackColor = true;
            this._startStopButton.Click += new System.EventHandler(this.startStopButton_Click);
            // 
            // _manualModeButton
            // 
            this._manualModeButton.Location = new System.Drawing.Point(475, 12);
            this._manualModeButton.Name = "_manualModeButton";
            this._manualModeButton.Size = new System.Drawing.Size(146, 23);
            this._manualModeButton.TabIndex = 1;
            this._manualModeButton.Text = "Enter Manual Mode";
            this._manualModeButton.UseVisualStyleBackColor = true;
            this._manualModeButton.Click += new System.EventHandler(this.manualModeButton_Click);
            // 
            // _emergencyButton
            // 
            this._emergencyButton.Location = new System.Drawing.Point(475, 41);
            this._emergencyButton.Name = "_emergencyButton";
            this._emergencyButton.Size = new System.Drawing.Size(146, 23);
            this._emergencyButton.TabIndex = 2;
            this._emergencyButton.Text = "Enter Emergency Mode";
            this._emergencyButton.UseVisualStyleBackColor = true;
            this._emergencyButton.Click += new System.EventHandler(this.emergencyButton_Click);
            // 
            // intersectionImage
            // 
            this.intersectionImage.Location = new System.Drawing.Point(21, 84);
            this.intersectionImage.Name = "intersectionImage";
            this.intersectionImage.Size = new System.Drawing.Size(600, 600);
            this.intersectionImage.TabIndex = 3;
            // 
            // _refreshTimer
            // 
            this._refreshTimer.Interval = 250;
            this._refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // _emergencySelection
            // 
            this._emergencySelection.FormattingEnabled = true;
            this._emergencySelection.Items.AddRange(new object[] {
            "A: Lane 1 -> Lane 2",
            "B: Lane 1 -> Lane 8",
            "C: Lane 3 -> Lane 4",
            "D: Lane 3 -> Lane 6",
            "E: Lane 5 -> Lane 7",
            "F: Lane 5 -> Lane 2",
            "G: Lane 7 -> Lane 8",
            "H: Lane 7 -> Lane 4"});
            this._emergencySelection.Location = new System.Drawing.Point(310, 43);
            this._emergencySelection.Name = "_emergencySelection";
            this._emergencySelection.Size = new System.Drawing.Size(159, 21);
            this._emergencySelection.TabIndex = 4;
            // 
            // IntersectionDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 700);
            this.Controls.Add(this._emergencySelection);
            this.Controls.Add(this.intersectionImage);
            this.Controls.Add(this._emergencyButton);
            this.Controls.Add(this._manualModeButton);
            this.Controls.Add(this._startStopButton);
            this.Name = "IntersectionDisplay";
            this.Text = "My Intersection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IntersectionDisplay_FormClosing);
            this.Load += new System.EventHandler(this.IntersectionDisplay_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _startStopButton;
        private System.Windows.Forms.Button _manualModeButton;
        private System.Windows.Forms.Button _emergencyButton;
        private System.Windows.Forms.Panel intersectionImage;
        private System.Windows.Forms.Timer _refreshTimer;
        private System.Windows.Forms.ComboBox _emergencySelection;
    }
}

