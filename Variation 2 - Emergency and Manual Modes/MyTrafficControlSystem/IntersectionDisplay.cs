﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using MyTrafficControlSystem.States;
using TrafficControl;

namespace MyTrafficControlSystem
{
    public partial class IntersectionDisplay : Form
    {
        private readonly TrafficController _trafficController;
        private readonly List<CircleTrafficLight> _trafficLights = new List<CircleTrafficLight>();
        private Bitmap _baseImage;
        private int _padding;
        private int _roadLength;
        private int _roadWidth;
        private int _laneWidth;
        private int _laneCenter;
        private int _roadMiddle;
        private int _roadCorner;
        private int _lightSpacing  = 3;
        private int _lightSize;

        public IntersectionDisplay()
        {
            InitializeComponent();

            _trafficController = new TrafficController(new StopAll1());
            SetupDrawingParameters();
            SetupTrafficLights();
            SetupBaseImage();
            DisplayIntersectionImage(_baseImage);
        }

        private void IntersectionDisplay_Load(object sender, EventArgs e)
        {
            _refreshTimer.Start();
        }

        private void IntersectionDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            _refreshTimer.Stop();
            _trafficController.Stop();
        }

        private void SetupDrawingParameters()
        {
            _padding = 20;
            _roadLength = intersectionImage.Height - _padding * 2;
            _roadWidth = _roadLength / 3;
            _laneWidth = _roadWidth / 2;
            _laneCenter = _laneWidth / 2;
            _roadMiddle = _roadLength / 2;
            _roadCorner = (_roadLength - _roadWidth) / 2;
            _lightSize = (_laneWidth - _lightSpacing*5)/3;
        }

        private void SetupTrafficLights()
        {
            // Lane 1
            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.LimeGreen,
                            _padding + _roadCorner,
                            _padding + _roadMiddle + _lightSpacing,
                            _lightSize,
                            typeof(AllowABCD), typeof(EmergencyA), typeof(EmergencyB)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Yellow,
                            _padding + _roadCorner,
                            _padding + _roadMiddle + _lightSpacing*2 + _lightSize,
                            _lightSize,
                            typeof(SlowABCD)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Red,
                            _padding + _roadCorner,
                            _padding + _roadMiddle + _lightSpacing * 3 + _lightSize*2,
                            _lightSize,
                            typeof(StopAll1), typeof(StopAll2),
                            typeof(AllowEFGH), typeof(SlowEFGH),
                            typeof(ManualFlashingOn),
                            typeof(EmergencyC), typeof(EmergencyD), typeof(EmergencyE),
                            typeof(EmergencyF), typeof(EmergencyG), typeof(EmergencyH)));

            // Lane 3
            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.LimeGreen,
                             _padding + _roadCorner + _roadWidth - _lightSize,
                            _padding + _roadCorner + _lightSpacing * 3 + _lightSize * 2,
                            _lightSize,
                            typeof(AllowABCD), typeof(EmergencyC), typeof(EmergencyD)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Yellow,
                            _padding + _roadCorner + _roadWidth - _lightSize,
                            _padding + _roadCorner + _lightSpacing * 2 + _lightSize,
                            _lightSize,
                            typeof(SlowABCD)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Red,
                             _padding + _roadCorner + _roadWidth - _lightSize,
                            _padding + _roadCorner + _lightSpacing,
                            _lightSize,
                            typeof(StopAll1), typeof(StopAll2),
                            typeof(AllowEFGH), typeof(SlowEFGH),
                            typeof(ManualFlashingOn),
                            typeof(EmergencyA), typeof(EmergencyB), typeof(EmergencyE),
                            typeof(EmergencyF), typeof(EmergencyG), typeof(EmergencyH)));

            // Lane 5
            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.LimeGreen,
                            _padding + _roadCorner + _lightSpacing * 3 + _lightSize * 2,
                            _padding + _roadCorner,
                            _lightSize,
                            typeof(AllowEFGH), typeof(EmergencyE), typeof(EmergencyF)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Yellow,
                            _padding + _roadCorner + _lightSpacing * 2 + _lightSize,
                            _padding + _roadCorner,
                            _lightSize,
                            typeof(SlowEFGH)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Red,
                            _padding + _roadCorner + _lightSpacing,
                            _padding + _roadCorner,
                            _lightSize,
                            typeof(StopAll1), typeof(StopAll2),
                            typeof(AllowABCD), typeof(SlowABCD),
                            typeof(ManualFlashingOn),
                            typeof(EmergencyA), typeof(EmergencyB), typeof(EmergencyC),
                            typeof(EmergencyD), typeof(EmergencyG), typeof(EmergencyH)));

            // Lane 7
            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.LimeGreen,
                            _padding + _roadMiddle + _lightSpacing,
                            _padding + _roadCorner + _roadWidth - _lightSize,
                            _lightSize,
                            typeof(AllowEFGH), typeof(EmergencyG), typeof(EmergencyH)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Yellow,
                            _padding + _roadMiddle + _lightSpacing * 2 + _lightSize,
                            _padding + _roadCorner + _roadWidth - _lightSize,
                            _lightSize,
                            typeof(SlowEFGH)));

            _trafficLights.Add(new CircleTrafficLight(_trafficController,
                            Color.Red,
                            _padding + _roadMiddle + _lightSpacing * 3 + _lightSize * 2,
                            _padding + _roadCorner + _roadWidth - _lightSize,
                            _lightSize,
                            typeof(StopAll1), typeof(StopAll2),
                            typeof(AllowABCD), typeof(SlowABCD),
                            typeof(ManualFlashingOn),
                            typeof(EmergencyA), typeof(EmergencyB), typeof(EmergencyC),
                            typeof(EmergencyD), typeof(EmergencyE), typeof(EmergencyF)));

        }

        private void SetupBaseImage()
        {
            _baseImage = new Bitmap(intersectionImage.Width, intersectionImage.Height);

            var centerLinePen = new Pen(Color.White, 8);
            var laneFont = new Font(FontFamily.GenericSansSerif, 30, FontStyle.Regular);

            var graphics = Graphics.FromImage(_baseImage);

            graphics.Clear(Color.LightGray);
            graphics.FillRectangle(Brushes.Black, _padding + _roadCorner, _padding, _roadWidth, _roadLength);
            graphics.FillRectangle(Brushes.Black, _padding, _padding + _roadCorner, _roadLength, _roadWidth);
            graphics.DrawLine(centerLinePen, _padding + _roadMiddle, _padding, _padding + _roadMiddle, _padding + _roadCorner);
            graphics.DrawLine(centerLinePen, _padding + _roadMiddle, _padding + _roadCorner + _roadWidth, _padding + _roadMiddle, _padding + _roadLength);
            graphics.DrawLine(centerLinePen, _padding, _padding + _roadMiddle, _padding + _roadCorner, _padding + _roadMiddle);
            graphics.DrawLine(centerLinePen, _padding + _roadCorner + _roadWidth, _padding + _roadMiddle, _padding + _roadLength, _padding + _roadMiddle);


            DrawTextCentered(graphics, "1", laneFont, Brushes.White, _padding*3, _padding + _roadMiddle + _laneCenter);
            DrawTextCentered(graphics, "2", laneFont, Brushes.White, _roadLength - _padding, _padding + _roadMiddle + _laneCenter);
            DrawTextCentered(graphics, "3", laneFont, Brushes.White, _roadLength - _padding, _padding + _roadCorner + _laneCenter);
            DrawTextCentered(graphics, "4", laneFont, Brushes.White, _padding*3, _padding + _roadCorner + _laneCenter);
            DrawTextCentered(graphics, "5", laneFont, Brushes.White, _padding + _roadCorner + _laneCenter, _padding*3);
            DrawTextCentered(graphics, "6", laneFont, Brushes.White, _padding + _roadCorner + _laneCenter, _roadLength - _padding);
            DrawTextCentered(graphics, "7", laneFont, Brushes.White, _padding + _roadMiddle + _laneCenter, _roadLength - _padding);
            DrawTextCentered(graphics, "8", laneFont, Brushes.White, _padding + _roadMiddle + _laneCenter, _padding * 3);
        }

        private void DrawTextCentered(Graphics graphics, string text, Font font, Brush brush, int x, int y)
        {
            var size = graphics.MeasureString(text, font);
            var left = x - size.Width/2;
            var top = y - size.Height/2;

            graphics.DrawString(text, font, brush, left, top);
        }

        private Image CreateIntersectionImage()
        {
            var image = new Bitmap(_baseImage);
            var graphics = Graphics.FromImage(image);

            foreach (var trafficLight in _trafficLights)
                trafficLight.Draw(graphics);

            return image;
        }

        private void DisplayIntersectionImage(Image image)
        {
            SuspendLayout();
            intersectionImage.CreateGraphics().DrawImageUnscaled(image, 0, 0);
            ResumeLayout();
        }


        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            DisplayIntersectionImage(CreateIntersectionImage());
        }

        private void startStopButton_Click(object sender, EventArgs e)
        {
            if (_startStopButton.Text == @"Start")
            {
                _trafficController.Start();
                _startStopButton.Text = @"Stop";
            }
            else
            {
                _trafficController.Stop();
                _startStopButton.Text = @"Start";
            }
        }

        private void manualModeButton_Click(object sender, EventArgs e)
        {
            if (_manualModeButton.Text == @"Enter Manual Mode")
            {
                _trafficController.EnterManualMode();
                _manualModeButton.Text = @"Exit Manual Mode";
            }
            else
            {
                _trafficController.ExitManualMode();
                _manualModeButton.Text = @"Enter Manual Mode";
            }
        }

        private void emergencyButton_Click(object sender, EventArgs e)
        {
            if (_emergencyButton.Text == @"Enter Emergency Mode")
            {
                _trafficController.EnterEmergencyMode(GetEmergencySelectedState());
                _emergencyButton.Text = @"Exit Emergency Mode";
            }
            else
            {
                _trafficController.ExitEmergencyMode();
                _emergencyButton.Text = @"Enter Emergency Mode";
            }
        }

        private TrafficState GetEmergencySelectedState()
        {

            var flow = _emergencySelection.SelectedItem?.ToString().Substring(0, 1) ?? "A";
            TrafficState state = new EmergencyA();
            switch (flow)
            {
                case "B":
                    state = new EmergencyB();
                    break;
                case "C":
                    state = new EmergencyC();
                    break;
                case "D":
                    state = new EmergencyD();
                    break;
                case "E":
                    state = new EmergencyE();
                    break;
                case "F":
                    state = new EmergencyF();
                    break;
                case "G":
                    state = new EmergencyG();
                    break;
                case "H":
                    state = new EmergencyH();
                    break;
            }
            return state;
        }
    }
}
