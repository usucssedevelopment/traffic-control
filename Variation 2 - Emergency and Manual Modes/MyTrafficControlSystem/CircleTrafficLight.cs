﻿using System;
using System.Drawing;
using TrafficControl;

namespace MyTrafficControlSystem
{
    public class CircleTrafficLight : TrafficLight
    {
        private readonly Color _offColor;
        private readonly int _x;
        private readonly int _y;
        private readonly int _size;

        public CircleTrafficLight(TrafficController controller,
                Color color,
                int x,
                int y,
                int size,
                params Type[] onStates) : base(controller, PossibleTypes.Circle, color, onStates)
        {
            _x = x;
            _y = y;
            _size = size;
            _offColor = Color.FromArgb(80, color);
        }

        public void Draw(Graphics graphics)
        {
            var color = IsOn ? Color : _offColor;
            var brush = new SolidBrush(color);
            var pen = new Pen(Color.Gray);
            graphics.FillEllipse(brush, _x, _y, _size, _size);
            graphics.DrawEllipse(pen, _x, _y, _size, _size);
        }
    }
}
