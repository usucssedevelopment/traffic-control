﻿namespace TrafficControl
{
    public abstract class TrafficState
    {
        protected int SecondsRemaining;

        protected TrafficState(int secondsToWaitInState)
        {
            SecondsRemaining = secondsToWaitInState;
        }

        public TrafficState ProcessElapsingTime(int seconds)
        {
            SecondsRemaining -= seconds;
            return (SecondsRemaining <= 0) ? GetNextState() : this;
        }

        public virtual TrafficState EnterManualMode()
        {
            return new ManualFlashingOff() { PreviousState = this };
        }

        public virtual TrafficState ExitManualMode() { return this;  }

        public TrafficState EnterEmergencyMode(TrafficState forState)
        {
            var newEmergencyState = forState as Emergency;

            if (GetType() == forState.GetType() || newEmergencyState == null) return this;

            newEmergencyState.PreviousState = this;
            return newEmergencyState;
        }

        public virtual TrafficState ExitEmergencyMode() { return this; }

        protected abstract TrafficState GetNextState();
    }
}
