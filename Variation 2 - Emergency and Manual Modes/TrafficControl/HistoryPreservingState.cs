﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficControl
{
    public abstract class HistoryPreservingState : TrafficState
    {
        public TrafficState PreviousState { get; set; }

        protected HistoryPreservingState(int secondsToWaitInState) : base(secondsToWaitInState) { }
    }
}
