﻿namespace TrafficControl
{
    public abstract class Emergency : HistoryPreservingState
    {
        protected Emergency() : base(int.MaxValue) { }

        public override TrafficState EnterManualMode()
        {
            return this;
        }

        public override TrafficState ExitManualMode()
        {
            return this;
        }

        public override TrafficState ExitEmergencyMode()
        {
            return PreviousState;
        }

        protected override TrafficState GetNextState() { return this; }
    }
}
