﻿namespace TrafficControl
{
    public class ManualFlashingOn : HistoryPreservingState
    {
        public ManualFlashingOn() : base(1) { }

        public override TrafficState EnterManualMode()
        {
            return this;
        }

        public override TrafficState ExitManualMode()
        {
            return PreviousState;
        }

        public override TrafficState ExitEmergencyMode()
        {
            return this;
        }

        protected override TrafficState GetNextState()
        {
            return new ManualFlashingOff() {PreviousState = PreviousState};
        }
    }
}
