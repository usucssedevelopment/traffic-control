﻿using System.Collections.Generic;
using System.Threading;

namespace TrafficControl
{
    public class TrafficController
    {
        private readonly TrafficState _initialState;
        private readonly List<TrafficLight> _observers = new List<TrafficLight>();
        private readonly object _myLock = new object();
        private Thread _myThread;

        public TrafficController(TrafficState initialState)
        {
            _initialState = initialState;
        }

        public void Subscribe(TrafficLight observer)
        {
            lock (_myLock)
            {
                if (!_observers.Contains(observer))
                    _observers.Add(observer);
            }
        }

        public void Unsubscribe(TrafficLight observer)
        {
            lock (_myLock)
            {
                if (_observers.Contains(observer))
                    _observers.Remove(observer);
            }
        }

        public void Start()
        {
            if (IsRunning)
                Stop();

            IsRunning = true;
            CurrentState = _initialState;
            Notify();

            _myThread = new Thread(Run);
            _myThread.Start();
        }

        public void Stop()
        {
            IsRunning = false;
        }

        public TrafficState CurrentState { get; private set; }
        public bool IsRunning { get; private set; }

        public void EnterManualMode()
        {
            CurrentState = CurrentState.EnterManualMode();
            Notify();
        }

        public void ExitManualMode()
        {
            CurrentState = CurrentState.ExitManualMode();
            Notify();
        }

        public void EnterEmergencyMode(TrafficState forState)
        {
            CurrentState = CurrentState.EnterEmergencyMode(forState);
            Notify();
        }

        public void ExitEmergencyMode()
        {
            CurrentState = CurrentState.ExitEmergencyMode();
            Notify();
        }

        protected void Notify()
        {
            lock (_myLock)
            {
                foreach (var observer in _observers)
                    observer.Update(CurrentState);
            }
        }

        private void Run()
        {
            while (IsRunning)
            {
                Thread.Sleep(1000);
                var newState = CurrentState.ProcessElapsingTime(1);
                if (newState == CurrentState) continue;

                CurrentState = newState;
                Notify();
            }
        }
    }
}
