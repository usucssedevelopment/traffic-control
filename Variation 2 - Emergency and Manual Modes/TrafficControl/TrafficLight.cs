﻿using System;
using System.Drawing;
using System.Linq;

namespace TrafficControl
{
    public class TrafficLight : IDisposable
    {
        public enum PossibleTypes { Circle, LeftArrow, Walk, DontWalk }

        private readonly Type[] _onStates;
        private readonly TrafficController _controller;

        public PossibleTypes Type { get; private set; }

        public Color Color { get; private set; }

        public bool IsOn { get; private set; }

        public TrafficLight(TrafficController controller,
                            PossibleTypes type,
                            Color color,
                            params Type[] onStates)
        {
            Type = type;
            Color = color;
            _onStates = onStates;
            _controller = controller;
            _controller.Subscribe(this);

        }

        public void Update(TrafficState state)
        {
            IsOn = _onStates.Contains(state.GetType());
        }

        public void Dispose()
        {
            _controller.Unsubscribe(this);
        }
    }
}
