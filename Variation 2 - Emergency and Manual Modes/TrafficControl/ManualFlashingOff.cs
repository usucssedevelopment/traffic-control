﻿namespace TrafficControl
{
    public class ManualFlashingOff : HistoryPreservingState
    {
        public ManualFlashingOff() : base(1) {}

        public override TrafficState EnterManualMode()
        {
            return this;
        }

        public override TrafficState ExitManualMode()
        {
            return PreviousState;
        }

        public override TrafficState ExitEmergencyMode()
        {
            return this;
        }

        protected override TrafficState GetNextState()
        {
            return new ManualFlashingOn() {PreviousState = PreviousState};
        }
    }
}
